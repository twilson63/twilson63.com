---
title: twilson63 notes auth0
description: setting up auth0
---

## Auth0 Setup Notes

Setting up Auth0 is not easy, but there are things you have to remember to do in the portal to get 
things to work right for a particular setup. Auth0 is good about reminding you, but I want to 
write it down, so when I forget in the future, I have a good reference.

1. create account/tenant

Using Auth0 for a application, start fresh with a new account or tenant for auth0

2. setup application

Create a new application and go through the process to add auth0 to your web application.


3. setup users, roles, permissions

Create a new user for your application using the connections section, and go ahead and verify the user.
Turn the ability to signup users off.

In the app_metadata set a new node called "tenant" and specify the default tenant.

4. create API

Create an API record for your application api, add permissions that will be used for that API, turn RBAC on and add permissions to the access token.

5. Create rule to add app_metadata

``` js

function (user, context, callback) {
  //if (context.connection === 'User-Password-Authentication') {
  context.idToken['https://pets.dev/tenant'] = user.app_metadata.tenant || 'store1';
  //}

  callback(null, user, context);
}
```

---

## Application and API 

After setting everything up in the Auth0 portal, you now have to set everything up in your application and API.

* Add `@auth0/auth0-spa-js` to your project
* Add a helper file for your given framework
* Go through the workflow to login/logout
* Secure your API with `express-jwt and jwks-rsa`
* Secure each endpoint
* Use the accessToken data to get tenant and permissions for each endpoint


```


