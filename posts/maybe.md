---
title: Maybe
---

## What is safe code?

3/9/2020

Why does software have so many secuirity vunerabilities? 

> Is the code I write secure? Is it safe? 

How can I improve on my code to make sure it is being used as intended?

Options

* Use type safe languages
* Use linters
* leverage testing

Type safe languages help when being explicit about the types being passed in, but types can not check boundaries. Linters are great, but when my function is being called by another source, they may not leverage linting. As far as testing, it is great, but you can spend a significant time trying to cover all the edge cases when it comes to boundaries. With all the solutions above there will always be holes and trade offs. 

My goal in the end is to create code that properly constrains its inputs in the following three ways:

* check the Type or Types of a parameter
* validate the shape of an object parameter
* check the bounds of the value of a parameter ie. string can only be 50 chars 

Applying these checks to each function can become tedious and make my code hard to read.

In functional programming there is a tool that can help wrap a function and give me a separate area in my code to perform these checks. The tool is called a `Maybe`. This `Maybe` tool allows me to define a predicate function for each argument that is being passed into my function.

> A `predicate` function is an unary (1 argument) function that returns a true or a false.

Within this `predicate` function, I can check for type(s), shape, and bounds, and return true if the argument satisfies my checks, and return false if the argument does not. When true, the function that is called returns the proper result wrapped in a Just Maybe object, and when false it retuns a Nothing Maybe object, both of these unwrap the value with an `option` method. The option method takes a default value so if Nothing is returned then the default value will be returned.

Why is this a big deal?

If I am writing api's or functions that others may use, even with good documentation and all the great intentions, my future self and other software engineers may pass the wrong parameter values to my functions and this will cause the internal expectations of my functions to be invalid.

Here is a very simple example:

``` js
function inc(n) {
  return n + 1
}
```

If I call this function `inc(2)` I will get the expected result of `3`, but if I call this function `inc('2')`, I will get the unexpected result of `21`. It does not throw an error or let the user know that they are calling the function with the wrong type. 

> With this simple contrieved example I might question the importance of this issue, but when working with the DOM and other transformational tools from inputs to values, numbers represented as strings is a common occurance and source of bugs. 

I can resolve this problem manually using the `typeof` keyword in Javascript. 

``` js
function inc(n) {
  if (typeof n !== 'number') { throw new Error('not a number') }
  return n + 1
}
```

For more complicated scenerios this would be tedious and it would make the code hard to read, it would require a lot of boilierplate. We could replace this with a language like typescript to leverage adding types to the arguments.

Lets look at how Maybe would work for this example.

``` js
import { Maybe } from 'crocks'

function inc(n) {
  return n + 1
}

const isNumber = n => typeof n === 'number' ? Maybe.Just(n) : Maybe.Nothing()
const safeInc = n => isNumber(n).map(inc).option(0)
```

Here the `safeInc` function isolates the argument check in an isNumber function that returns a Maybe, then you map the Maybe to the `inc` function, if the `isNumber` function returns a `Just` then it will pass the value to the `inc` function, otherwise it will return `Nothing`. If the value is valid the correct result is returned if not a number is returned as it is unwrapped from the Maybe Object using the `option` method.

These type of Maybe checks are so common, the `crocks` library has created a set of `lift` functions, that take the predicate and function to return a safe function that returns a maybe.

``` js
import { safeLift } from 'crocks'

function inc(n) {
  return n + 1
}

// predicate function
const isNumber = n => typeof n === 'number'

const safeInc = n => safeLift(isNumber, inc)(n).option(0)
```

The `safeLift` function takes the predicate function and the target function and creates a Maybe and maps the target function to the value in the Maybe.

What if I have a function with more than one argument that I want to make safe using the Maybe?

With crocks there are liftA2 and liftA3 and liftN:

* liftA2 - takes two predicate function arguments and the target function.
* liftA3 - takes three predicate function arguments and the target function.

If you need more than three arguments, see liftN

## Demos



``` 

