const shell = require('shelljs')

const sh = shell.exec

sh(`mkdir -p dist`)
sh(`istyle air > dist/style.css`)

const pages = ['index', 'about', 'projects']

pages.forEach(page => {
  sh(`ipress ${page}.md > dist/${page}.html`)
})

sh(`mkdir -p dist/notes`)

const notes = ['index', 'auth0', 'theia']

notes.forEach(page => {
  sh(`ipress notes/${page}.md > dist/notes/${page}.html`)
})

/*
sh(`mkdir -p dist/posts`)

const posts = ['index', 'maybe']

posts.forEach(page => {
  sh(`ipress posts/${page}.md > dist/posts/${page}.html`)
})
*/



